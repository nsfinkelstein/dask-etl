from setuptools import setup

setup(
    name='dask_etl',
    version='0.1',
    description='ETL built on dask',
    url='gitlab.com/nsfinkelstein/dask_etl',
    packages=['dask_etl'],
    install_requires=[
        'aiofiles>=0.4.0',
        'asyncpg>=0.17.0',
        'dask[complete]>=2.12.0',
        'pandas>=1.0.1'
    ]
)
