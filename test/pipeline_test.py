import os
import pytest
import asyncio
import numpy as np
import pandas as pd

from dask_etl import run_query, run_graph, pyfeature, sqlfeature, reader, get_creation_query, load, run_tasks, \
    get_ancestors

# configuration
querydir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'queries')
dbconfig = {
    'port': 5432,
    'user': 'testuser',
    'host': '127.0.0.1',
    'password': 'testpassword',
    'database': 'testdb',
}


@pytest.fixture()
def testdb():
    asyncio.run(run_query(dbconfig, 'testsetup', querydir=querydir))
    yield None  # run function
    asyncio.run(run_query(dbconfig, 'testteardown', querydir=querydir))


def test_run_tasks(testdb):
    # define actors
    sql = sqlfeature(name='sqlfeature', querydir=querydir)
    py = pyfeature(transform=lambda x: x * 2, tablename='pyfeature', name='pyfeature', read_table='sqlfeature')
    sqlreader = reader(name='sqlreader', read_table='sqlfeature')
    pyreader = reader(name='pyreader', read_table='pyfeature')

    # define pipeline
    pipeline = {
        sql: set(),
        py: {'sqlfeature'},
        sqlreader: {'sqlfeature'},
        pyreader: {'pyfeature'},
    }

    # Put features into the database

    asyncio.run(run_graph(dependencies=pipeline, dbconfig=dbconfig))

    # If run_ancestors is False (as is by default), delete parents from the graph that we haven't been explicitly asked to run.
    # Check that pyreader is included in the results

    results = asyncio.run(run_tasks(tasks={py, pyreader}, dependencies=pipeline, dbconfig=dbconfig))

    expected = pd.DataFrame.from_records([(2, 1), (3, 2)], columns=['col1', 'col2'])
    pd.testing.assert_frame_equal(results['pyreader'], expected * 2)

    # If run_ancestors is True, check that sqlfeature is included (and None), but sqlreader was not included at all.
    results = asyncio.run(run_tasks(tasks={py, pyreader}, dependencies=pipeline, dbconfig=dbconfig, run_ancestors=True))

    assert results['sqlfeature'] is None
    assert 'sqlreader' not in results

    # If run_ancestors is False, check that pyfeature is not included at all

    results = asyncio.run(run_tasks(tasks={pyreader}, dependencies=pipeline, dbconfig=dbconfig, run_ancestors=False))

    assert 'pyfeature' not in results

def test_pipeline(testdb):
    # define actors
    sql = sqlfeature(name='sqlfeature', querydir=querydir)
    py = pyfeature(transform=lambda x: x * 2, tablename='pyfeature', name='pyfeature', read_table='sqlfeature')

    # define pipeline
    pipeline = {
        sql: set(),
        py: {'sqlfeature'},
        reader(name='sqlreader', read_table='sqlfeature'): {'sqlfeature'},
        reader(name='pyreader', read_table='pyfeature'): {'pyfeature'},
    }

    # run pipeline
    results = asyncio.run(run_graph(pipeline, dbconfig))

    # verify pipeline ran as expected
    expected = pd.DataFrame.from_records([(2, 1), (3, 2)], columns=['col1', 'col2'])

    # check results
    assert results['sqlfeature'] is None
    assert results['pyfeature'] is None
    pd.testing.assert_frame_equal(results['sqlreader'], expected)
    pd.testing.assert_frame_equal(results['pyreader'], expected * 2)

    # check database state
    pd.testing.assert_frame_equal(asyncio.run(run_query(dbconfig, '', read_table='sqlfeature')), expected)
    pd.testing.assert_frame_equal(asyncio.run(run_query(dbconfig, '', read_table='pyfeature')), expected * 2)


def test_bad_pyfeature(testdb):
    # bad query for reading in data
    py = pyfeature(transform=lambda _: None, tablename='pyfeature', name='badfeature', querydir=querydir)
    pipeline = {py: set()}

    with pytest.raises(Exception) as excinfo:
        asyncio.run(run_graph(pipeline, dbconfig))
    msg = 'query does not end by selecting data out of the database'
    assert msg in str(excinfo.value)

    # bad transform function
    py = pyfeature(transform=lambda _: None, tablename='pyfeature', name='badfeature', read_table='testtable');
    pipeline = {py: set()}

    with pytest.raises(Exception) as excinfo:
        asyncio.run(run_graph(pipeline, dbconfig))
    msg = 'function does not return a dataframe'
    assert msg in str(excinfo.value)


def test_query_creation(testdb):
    # handle semicolon and quote escaping properly, np.nan
    df = pd.DataFrame({
        'col1': ['with;semicolon', "with'single'quotes", None],
        'col2': [1.0, np.nan, None],
        'col3': [np.datetime64('2012-11-10T16:00:00'), np.datetime64(), None],
        'col4': [np.timedelta64(100, 'D'), np.timedelta64(), None],
    })
    asyncio.run(load(dbconfig, 'stringtest', df))
    read = asyncio.run(run_query(dbconfig, '', 'stringtest'))
    pd.testing.assert_frame_equal(df, read)


def test_invalid_query(testdb):
    with pytest.raises(Exception) as excinfo:
        asyncio.run(run_query(dbconfig, 'invalidwrite', querydir=querydir))
    msg1 = 'schema "numpy" does not exist'
    msg2 = 'create table numpy.test (col1 int, col2 int)'
    assert msg1 in str(excinfo.value) and msg2 in str(excinfo.value)


def test_valid_graph():
    py = pyfeature(transform=lambda x: x * 2, tablename='pyfeature', name='pyfeature', read_table='sqlfeature')
    bad_graph = {py: {'sqlfeature'}}
    with pytest.raises(Exception) as excinfo:
        asyncio.run(run_graph(bad_graph, dbconfig, querydir))
    msg = r"depends on task(s) {'sqlfeature'}, not defined in the pipeline"
    assert msg in str(excinfo.value)


def test_get_ancestors():
    sql1 = sqlfeature(name='sqlfeature1', querydir=querydir)
    sql2 = sqlfeature(name='sqlfeature2', querydir=querydir)
    sql3 = sqlfeature(name='sqlfeature3', querydir=querydir)
    sql4 = sqlfeature(name='sqlfeature4', querydir=querydir)
    sql5 = sqlfeature(name='sqlfeature5', querydir=querydir)
    pipeline = {
        sql1: set(),
        sql2: {'sqlfeature1'},
        sql3: {'sqlfeature2'},
        sql4: {'sqlfeature1', 'sqlfeature2'},
        sql5: {'sqlfeature1', 'sqlfeature2'},
    }
    # test multiple levels of ancestry
    final_dependencies = get_ancestors(tasks={sql3, sql5}, dependencies=pipeline)
    expected1 = {
        sql1: set(),
        sql2: {'sqlfeature1'},
        sql3: {'sqlfeature2'},
        sql5: {'sqlfeature1', 'sqlfeature2'},
    }
    assert final_dependencies == expected1

    # test intermediate ancestral subgraph
    final_dependencies = get_ancestors(tasks={sql2}, dependencies=pipeline)
    expected2 = {
        sql1: set(),
        sql2: {'sqlfeature1'}
    }

    assert final_dependencies == expected2
    # test no ancestors
    final_dependencies = get_ancestors(tasks={sql1}, dependencies=pipeline)
    expected3 = {
        sql1: set(),
    }

    assert final_dependencies == expected3
