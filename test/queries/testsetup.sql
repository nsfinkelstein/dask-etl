drop table if exists sqlfeature;
drop table if exists pyfeature;
drop table if exists testtable;

create table testtable (col1 int, col2 int);

insert into testtable (col1, col2) values (1, 1), (2, 2);
