# Dask Etl

Dask Etl is a framework for asynchronous, parallel postgres database-based ETL
pipeline.

At present, it can be used when raw data exists in a postgres database, and the
transformed data needs to be loaded back into the the same database. Other use
cases may be covered in the future.

Data can be transformed either in SQL or in Python. In the former case, a query
that selects data, manipulates it, and loads it back into the database is run
asynchronously on one of the workers in the Dask cluster (i.e. across cores, if
run on a single machine as by default). In the latter a select query is run
asynchronously on one worker, then a transform step is run synchronously on the
same worker, and finally the data is loaded back into the database
asychnronously. No data is transfered between workers at any point. Finally it
is also possible to read data out of the database as pandas dataframes
asynchronously. This is done in the main process to avoid transfering data. In
the future, the library may be extended to handle reading data onto multiple
dask workers to set-up for distributing computations.

Bugs reports and feature requests should be submitted through gitlab issues on
this repository.

## Installation

`dask_etl` requires python 3.8 or above. A conda virutal environment with python
3.8 can be created via `conda create --name <envname> python=3.8`.

Clone this repository, cd into it, then run `pip install --editable ./`. The
package is now installed on "editable" mode, which means that if you make
changes to the local source code, they will be picked up wherever this package
is imported.

## Usage

This package has two main concepts: "Actors", and "Pipelines". Please view the
test directory, and especially the `test_pipeline` test for sample usage.

### Actors

There are three types of "actors". They each have some combination of these
attributes: 

1. `name`: The name of the actor, which will be used to refer to it for reasons of
   tracking dependencies.

2. `querydir` (default `None`): The path to the location of the directory from which
   to read queries.
    
3. `read_table` (default `''`): The name of the table to read from the database.

4. `tablename`: The name of the table to write to the database.

5. `transform`: A function that takes a dataframe as input, transforms it, and
   returns a dataframe as output.
  

Note that one of `querydir` or `read_table` must be changed from the default. If
`read_table` is set away from the default, the table with the corresponding name
is read. If `querydir` is set away from the default, then the query with a name
corresponding to the name of the actor is found in that directory.

For example, if the name of the actor is `somefeature`, then the query that will
be read is `{querydir}/somefeature.sql`.

The `reader` actor will be run on the scheduler thread, to avoid the need to
move data around between workers. All other actors will be run in parallel on as
many cores as are available.

#### sqlfeature

Has attributes `name` and `querydir`. Reads and executes the sql query at
`{querydir}/{name}.sql`. 

Note that this query should:
* create a table in the database (using `select into` or similar), and 
* not select anything out of the database. 

#### pyfeature

Has all five attributes above. First, runs a query against the database. If
`read_table` is not `''`, that query just reads a table with the name given as
the `read_table` attribute. Otherwise, it runs a query at
`{querydir}/{name}.sql`. 

This query should:
* read a relation out of the database (not necessarily just an existing table), and 
* should not write anything into the database.

Then, the resulting dataframe is transformed using the `transform` function.

Finally, the transformed data is stored back in the database in a table called
`tablename`.

#### reader

It operates much as the `sqlfeature`, but there is the option to read an
existing table directly out of the database by setting the `read_table`
attribute to the name of that table. If, instead, a query is used to read a
relation out of the database, that query should live at `{querydir}/{name}.sql`,
and it should be a very simple query that does not perform any computations.

### Pipelines

A pipeline is just a python dictionary. The keys in the dictionary are the
actors themselves, and the values are the _names_ corresponding to the actors
that any given actor depends on.


#### Defining Pipelines

Here is an example pipeline:

```.py
    sql = sqlfeature(name='sqlfeature', querydir=querydir)
    py = pyfeature(transform=lambda x: x*2, tablename='pyfeature', name='pyfeature', read_table='sqlfeature')


    # define pipeline
    pipeline = {
        sql: set(),
        py: {'sqlfeature'},
        reader(name='sqlreader', read_table='sqlfeature'): {'sqlfeature'},
        reader(name='pyreader', read_table='sqlfeature'): {'pyfeature'},
    }
```

The actor called `sql`, which is a `sqlfeature` actor, depends on nothing. The
actor called `py`, which is a `pyfeature` actor, depends on `sql` having beeng
completed. This may be because, e.g., `sql` puts a table in the database that
`py` then uses.

Finally, each of those two has a `reader` actor, which reads the corresponding
table out of the database.

#### Running Pipelines

The pipeline can be run with the following command:

  `results = asyncio.run(run_graph(pipeline, dbconfig, querydir))`
  
It is also possible to run a subset of tasks in the pipeline using the
`run_tasks` coroutines. See the code for more details and optional arguments
(additional documentation will come).

## Development

The tests expect a postgres server to be running on `localhost` at port `5432`
with a user called `testuser`, a password called `testpassword`, and a database
called `testdb`. Any development that touches database interactions should run
existing tests, and add new tests to cover the new functionality. Tests can be
run with `pytest`.
