from collections import namedtuple

import dask.distributed as dd
import pandas as pd
import dask_etl
import aiofiles
import inspect
import asyncio
import asyncpg
import os

SEMICOLON = 'SEMICOLON'
SINGLEQUOTE = 'SINGLEQUOTE'

pyfeature = namedtuple('pyfeature', ['transform', 'tablename', 'name', 'read_table', 'querydir'], defaults=['', None])
sqlfeature = namedtuple('sqlfeature', ['name', 'read_table', 'querydir'], defaults=['', None])
reader = namedtuple('reader', ['name', 'read_table', 'querydir'], defaults=['', None])


async def run_query(dbconfig, name, read_table='', querydir=None, conn=None):
    assert read_table or querydir

    query = await get_query(read_table, name, querydir)
    result = await execute(dbconfig, query, conn)

    return result


async def get_query(read_table, query, querydir):
    if read_table:
        return 'select * from {};'.format(read_table)
    return await read_query(query, querydir)


async def execute(dbconfig, query, conn):
    close_conn = conn is None
    conn = conn or await asyncpg.connect(**dbconfig)

    subqueries = (
        q for q in (
        '\n'.join(
            q for q in query.split('\n')
            if q.strip() and q.strip()[:2] != '--'  # drop empty lines and comments
        )).split(';') if q.strip() and q.strip()[:2] != '--'
    )

    # put back escaped characters
    escaped_queries = (
        q.replace(SEMICOLON, ';').replace(SINGLEQUOTE, "''")
        for q in subqueries
    )

    # replace null values
    nulls = {'nan', "'NaT'", 'None'}
    nullified_queries = []
    for query in escaped_queries:
        for null in nulls:
            query = query.replace(null, 'null')
        nullified_queries.append(query)

    # assume select at end
    for subquery in nullified_queries:
        try:
            values = await conn.fetch(subquery)
        except Exception as e:
            if close_conn:
                await conn.close()
            msg = 'Original exception:\n{}\n\nInvalid query:\n{}'
            raise ValueError(msg.format(e, subquery))

    if values:
        select_portion = nullified_queries[-1]  # assume select at end
        prepared = await conn.prepare(select_portion)
        columns = [a.name for a in prepared.get_attributes()]

    if close_conn:
        await conn.close()

    if values:
        return pd.DataFrame.from_records(values, columns=columns)


async def load(dbconfig, tablename, dataframe, conn=None):
    query = dask_etl.get_creation_query(tablename, dataframe)
    return await execute(dbconfig, query, conn)


async def read_query(queryname, querydir):
    queryfile = os.path.join(querydir, queryname + '.sql')
    async with aiofiles.open(queryfile, 'r') as f:
        return await f.read()


class EtlActor(object):
    def __init__(self):
        self.extracted_data = None
        self.transformed_data = None
        self.conn = None

    async def extract(self, dbconfig, query, read_table, querydir):
        self.conn = await asyncpg.connect(**dbconfig)
        self.extracted_data = await run_query(dbconfig, query, read_table, querydir, self.conn)

        if self.extracted_data is None:
            await self.conn.close()
            msg = 'The following query does not end by selecting data out of the database'
            query = await get_query(read_table, query, querydir)
            raise ValueError('{}\n{}'.format(msg, query))

    def transform(self, transform):
        self.transformed_data = transform(self.extracted_data)

        if not isinstance(self.transformed_data, pd.DataFrame):
            msg = 'The transform function does not return a dataframe.'
            src = inspect.getsource(transform)
            raise ValueError('{}\n{}'.format(msg, src))

    async def load(self, dbconfig, tablename):
        await load(dbconfig, tablename, self.transformed_data, self.conn)
        await self.conn.close()


class CoroActor(object):
    async def coro(self, dbconfig, query, read_table, querydir):
        return await run_query(dbconfig, query, read_table, querydir)


async def run_etl(actor, client, dbconfig):
    """ Runs ETL, returns transformed data if return_transormed=True"""
    etl_actor = await client.submit(EtlActor, actor=True)

    # all state is stored on actor to prevent moving data around unnecessarily
    await etl_actor.extract(dbconfig, actor.name, actor.read_table, actor.querydir)

    # should wait for extract to complete, because internally uses its result
    await etl_actor.transform(actor.transform)

    # should wait for transform, coro should only return after transform called
    await etl_actor.load(dbconfig, actor.tablename)


async def run_coro(actor, client, dbconfig):
    coro_actor = await client.submit(CoroActor, actor=True)
    return await coro_actor.coro(dbconfig, actor.name, actor.read_table, actor.querydir)


async def run_reader(actor, client, dbconfig):
    # run on main thread to avoid moving data around
    return await run_query(dbconfig, actor.name, actor.read_table, actor.querydir)


def runnable(dependencies, results, running_tasks):
    return set(
        actor for actor, parents in dependencies.items()
        if parents.issubset(set(results))
        and actor.name not in (task.get_name() for task in running_tasks)
        and actor.name not in results
    )


async def run_graph(dependencies, dbconfig, client=None, waittime=.1):
    validate_dependencies(dependencies)
    shutdown_client = client is None
    client = client or await dd.Client(asynchronous=True)

    results = dict()
    running_tasks = set()
    all_tasks = set(d.name for d in dependencies)

    while set(results) != all_tasks:
        for actor in runnable(dependencies, results, running_tasks):
            # run readers on main thread to avoid data transfer
            runners = {
                sqlfeature: run_coro,
                pyfeature: run_etl,
                reader: run_reader,
            }
            coro = runners[type(actor)](actor, client, dbconfig)
            task = asyncio.create_task(coro, name=actor.name)

            print('about to run', task.get_name())
            running_tasks.add(task)

        for task in list(filter(lambda x: x.done(), running_tasks)):
            # TODO: add error handling for failed tasks
            print('completed', task.get_name())
            try:
                results[task.get_name()] = task.result()
            except Exception as e:
                # TODO: more robust recovery
                if shutdown_client:
                    await client.shutdown()
                raise e

            running_tasks.remove(task)

        # TODO: ensure newly added tasks start before continuing
        await asyncio.sleep(waittime)

    if shutdown_client:
        await client.shutdown()

    return results


def get_ancestors(tasks, dependencies):
    name_task_map = {task.name: task for task in dependencies}
    final_dependencies = dict()
    all_parents = [dependencies[task] for task in tasks] + [set(task.name for task in tasks)]
    unresolved_dependencies = set.union(*all_parents) if all_parents else set()
    resolved_dependencies = set()
    while unresolved_dependencies:
        task_name = unresolved_dependencies.pop()
        if task_name not in resolved_dependencies:
            final_dependencies[name_task_map[task_name]] = dependencies[name_task_map[task_name]]
            unresolved_dependencies.update(dependencies[name_task_map[task_name]])
            resolved_dependencies.update({task_name})

    return final_dependencies


async def run_tasks(tasks, dependencies, dbconfig, run_ancestors=False, client=None, waittime=.1):
    task_names = {task.name for task in tasks}
    if not run_ancestors:
        final_dependencies = {task: {dep for dep in dependencies[task] if dep in task_names} for task in tasks}
    else:
        final_dependencies = get_ancestors(tasks=tasks, dependencies=dependencies)

    return await run_graph(dependencies=final_dependencies, dbconfig=dbconfig, client=client,
                           waittime=waittime)


def validate_dependencies(deps):
    all_tasks = set(d.name for d in deps)
    for actor, parents in deps.items():
        missing_tasks = parents - all_tasks
        if missing_tasks:
            msg = 'task {} depends on task(s) {}, not defined in the pipeline'
            raise ValueError(msg.format(actor.name, missing_tasks))
