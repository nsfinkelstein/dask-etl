import numpy as np

from .etl import SEMICOLON, SINGLEQUOTE


def get_creation_query(name, df):
    if len(df) == 0:
        raise ValueError('Cannot write empty dataframe')

    deletion = 'drop table if exists {} cascade;'.format(name)
    cols_types = ','.join('{} {}'.format(col, get_col_type(col, df)) for col in df.columns)
    creation = 'create table {} ({});'.format(name, cols_types)

    cols = ','.join(c for c in df.columns)
    records = df.to_records(index=False)

    # escape characters that can trip up later query processing
    def seconds(x):
        if str(x) == 'NaT':
            return None

        return '{} seconds'.format(x / np.timedelta64(1000000000, 'ns'))

    cleaners = {
        str: lambda x: x.replace("'", SINGLEQUOTE).replace(';', SEMICOLON),
        np.datetime64: np.datetime_as_string,
        np.timedelta64: seconds,
    }

    escaped = (
        tuple(cleaners.get(type(x), lambda x: x)(x) for x in record)
        for record in records
    )
    data = ','.join(str(record) for record in escaped)
    insertion = 'insert into {} ({}) values {};'.format(name, cols, data)

    return '{}\n\n{}\n\n{}'.format(deletion, creation, insertion)


def get_col_type(col, df):
    type_str = str(df[col].dtype).lower()

    if 'int' in type_str:
        return 'int'

    if 'float' in type_str:
        return 'float'

    if 'timedelta' in type_str:
        return 'interval'

    if 'datetime' in type_str:
        return 'timestamp without time zone'

    return 'text'
